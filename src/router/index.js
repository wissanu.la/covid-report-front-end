import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ChartMonth from "../views/ChartMonth.vue"
import Top5Province from "../views/Top5Province.vue"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/chart',
    name: 'ChartMonth',
    component: ChartMonth
  },
  {
    path: '/top5',
    name: 'Top5Province',
    component: Top5Province
  }
]

const router = new VueRouter({
  routes
})

export default router
